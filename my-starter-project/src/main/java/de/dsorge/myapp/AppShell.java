package de.dsorge.myapp;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.PWA;

@PWA(name = "Vaadin Application", shortName = "Vaadin App", description = "This is an example Vaadin application.")
public class AppShell implements AppShellConfigurator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9024792817295435519L;

}
