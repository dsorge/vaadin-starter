package de.dsorge.myapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.vaadin.flow.i18n.I18NProvider;

@Component
public class CustomI18NProvider implements I18NProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8945849275213593603L;

	private static final Logger LOG = LoggerFactory.getLogger(CustomI18NProvider.class);

	@Override
	public List<Locale> getProvidedLocales() {
		final List<Locale> providedLocales = new ArrayList<>();
		providedLocales.add(Locale.GERMAN);
		providedLocales.add(Locale.ENGLISH);
		return providedLocales;
	}

	@Override
	public String getTranslation(final String key, final Locale locale, final Object... params) {
		if (LOG.isInfoEnabled()) {
			LOG.info(String.format("Translate key %s", key));
		}
		return "";
	}

}
