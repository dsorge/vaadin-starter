package de.dsorge.myapp.service;

import java.io.Serializable;

import org.springframework.stereotype.Service;

import com.vaadin.flow.spring.annotation.VaadinSessionScope;

@VaadinSessionScope
@Service
public class GreetService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -534859963566421462L;

	public String greet(String name) {
		if (name == null || name.isEmpty()) {
			return "Hello anonymous user";
		} else {
			return "Hello " + name;
		}
	}

}
